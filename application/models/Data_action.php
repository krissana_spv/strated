<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_action extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }

	public function personal_data()
	{	
		$query  = $this->db->select('*')
                         ->from('user')
                         ->get()
                         ->result_array();
        return $query;

    }
    
    public function contact_data()
	{	
		
    }
    
    public function contact_find($station_id)
	{	
		$query  = $this->db->select('*')
                         ->from('contact')
                         ->where('status' , 1)
                         ->where('id' , $station_id)
                         ->get()
                         ->row_array();
        return $query;
    }
    
    public function edit_data()
	{	
        
    }
    
    public function add_data($data)
	{	
        $result = $this->db->insert('contact', $data);
        return $result;
    }
    
      public function delete_data($id)
	{	
        $result = $this->db->where('id', $id)
                            ->delete('contact');
        return $result;
	}

	
}