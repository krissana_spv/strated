<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_check extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }

	public function usercheck($data)
	{	
		$query  = $this->db->select('*')
                         ->from('user')
                         ->where('id_number', $data['id_number'])
                         ->get()
                         ->row_array();
        return $query;

	}

	public function add_newuser($data)
	{	
		// 	echo "<pre>";
  	//	print_r($data);
  	//  echo "</pre>";
		$this->db->insert('user', $data);

	}

	public function usercheck_status($data)
	{	
		$resultstatus  = $this->db->select('*')
                         ->from('user')
                         ->where('id_number', $data['id_number'])
                         ->where('user_status', '1')
                         ->get()
                         ->row_array();
    return $resultstatus;

	}

	public function userdata($data)
	{	
		$query  = $this->db->select('*')
                         ->from('user')
                         ->where('id', $data['id'])
                         ->get()
                         ->row_array();
        return $query;

	}
	
}