<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_con extends CI_Controller {
	
	function __construct() {
   	parent:: __construct();

   	$this->load->model('Account_check');
   	$this->load->model('Data_action');
	  }
	  
	public function index()
	{	
		$data['userdata'] = $this->Account_check->userdata($_SESSION['logged_in']);
		$data['personal'] = $this->Data_action->personal_data();
		$this->template->load('template','dashboard',$data);
	}

	public function table()
	{	
		
	}

	public function edit()
	{	
		$data['contact'] = $this->Data_action->contact_find($station_id);
		$data['userdata'] = $this->Account_check->userdata($_SESSION['logged_in']);
		$this->template->load('template','card_edit',$data);
	}

	public function edit_data()
	{	
		$data = array(  
					'name' => $this->input->post('name'),
					'type' => $this->input->post('type'),
					'district' => $this->input->post('district'),
					'province' => $this->input->post('province'),
					'phone' => $this->input->post('phone')
					);
		$result = $this->Data_action->edit_data($data,$_POST['id']);
		if($result != 0)
		 {redirect( base_url('Main_con/table') );}
	}

	public function add()
	{	
		$data['userdata'] = $this->Account_check->userdata($_SESSION['logged_in']);
		$this->template->load('template','card_import',$data);
	}

	public function add_data()
	{	
		$data = array();

		if($result != 0)
		 {redirect( base_url('Main_con/table') );}
	}

	public function contact_delete($id)
	{	
		$result = $this->Data_action->delete_data($id);
		if($result != 0)
		{redirect( base_url('Main_con/table') );}
	}

}
