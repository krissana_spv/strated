<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account1 extends CI_Controller {

	function __construct() {
   	parent:: __construct();

   	//$this->load->model('Account_check');
   	
  	}
	
// checklogin and load index
	public function index()
	{
   			$this->load->view('start_page');
	}

  public function login()
  {
    if(isset($_SESSION['logged_in']['user_id']) ){
        redirect(base_url('Account/main'));
    }
    else{
        $this->load->view('login');
    }
  }

public function main()
	{
        redirect(base_url('Main_con'));
	}

	public function authen()
    {   
    //print_r($_POST);
        $headers = [
            "Content-Type: application/json"
        ];

        $data = [
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        ];      
        $data_encode = json_encode($data);

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, 'http://smartoffice.royalrain.go.th/services/web/index.php/user/authen');
        curl_setopt( $ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_encode);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $data['result'] = json_decode($result, true);

        if($data['result']['success'] == 1) {

            $qr_login = FALSE;
             $this->check_user($data['result']['data'], $qr_login);
            
            // echo "<pre>";
            // print_r($data['result']);
            // echo "</pre>";
            
        } else {
            echo "<script>";
			echo "alert('ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง');";
			echo "window.history.back();";
			echo "</script>";
        }

    }

    public function check_user($data, $qr_login) {
             $result = $this->Account_check->usercheck($data);
             if ($result > 0) {
                    $this->create_session($data, $qr_login);     
             }
            else{
                $data = array(
                'id_number' => $data['id_number'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'user_status' => '1'
                );
                $resultadd = $this->Account_check->add_newuser($data);
                if ($resultadd = 1) {
                    $this->create_session($data, $qr_login);
                }else{
                    echo "<script>";
                    echo "alert('กรุณาติดต่อผู้ดูแลระบบ');";
                    echo "window.history.back();";
                    echo "</script>";
                }
                
            }

    }

    public function create_session($data, $qr_login){
        $userdata = $this->Account_check->usercheck($data);
        $session_data = array(
        'id'            => $userdata['id']
        );
        $this->session->set_userdata('logged_in', $session_data);

        if($qr_login === TRUE) {
            echo "ok";
            }else {
            $this->main();
            }
               
    }

    public function qr_data() {

        $data['USER_ID']        = $this->input->post('USER_ID');
		$data['first_name']		= $this->input->post('first_name');
		$data['id_number']		= $this->input->post('id_number');
        $qr_login = TRUE;
        $this->check_user($data, $qr_login);
        
	}

    public function logout() {
        $sess_array = array(
            'id' => ''
        );
        $this->session->unset_userdata('logged_in', $sess_array);
        $this->session->unset_userdata('url');
        redirect(base_url());
    }

    public function forget_password() {

        $headers = [
            "Content-Type: application/json"
        ];

        $data = [
            'email' => $this->input->post('email')
        ];      
        $data_encode = json_encode($data);

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL,'http://smartoffice.royalrain.go.th/services/web/index.php/user/forget_password' );
        curl_setopt( $ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_encode);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($result, true);

        if($json['success'] == 1) {
                    echo "<script>";
                    echo "alert('กรุณาตรวจสอบอีเมลล์ของท่าน');";
                    echo "window.history.back();";
                    echo "</script>";
        }else {
            echo "<script>";
                    echo "alert('อีเมลล์ไม่ถูกต้อง');";
                    echo "window.history.back();";
                    echo "</script>";
        }
    }




	
}
