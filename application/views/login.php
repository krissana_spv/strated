<!DOCTYPE html>
<html lang="en">
<style>
  blue{
    color: royalblue;
  }
  black{
    color: black;
  }
  body{
    background : url(img/clound.jpg) ;
    background-repeat: no-repeat;
    background-size: 100% 100%;
  }
</style>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" sizes="192x192" href="<?php echo base_url(); ?>('img/wing.png'); " >
  <title>Radar</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url(); ?>sbadmin2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- Custom styles for this template-->
  <link href="<?php echo base_url(); ?>sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">

  <script src="<?php echo base_url('sbadmin2/vendor/js/jquery-3.2.1.min.js'); ?>"></script>

  <!-- generate QR Code -->
  <script src="<?php echo base_url('sbadmin2/js/qrcode.min.js'); ?>"></script>
</head>

<script type="text/javascript">
  var res;
        function generate_qr() {
      $('#usernamlogin').hide();
      $('#qrcodelogin').show();
      $.ajax({
        method: "POST",
        url: "http://smartoffice.royalrain.go.th/services/web/index.php/user/generate_qr",
        cache: false,
        success: function(response) {
                    res = response;

                    return res;
                }
      })
      .done(function() {
          console.log(res.data.qrcode);
          makeCode();
          intervalVerify();
      });  

      var qrcode = new QRCode("qrcode");

          function makeCode () {      
              var elText = res.data.qrcode;
              qrcode.makeCode(elText);
          }  

    }

    var data;
    var loop;
    function intervalVerify() {
        var interV = setInterval(function() {
            var msg = $.ajax({
                url : "http://smartoffice.royalrain.go.th/services/web/index.php/user/verify_qr",
                data: JSON.stringify({ "qrcode": res.data.qrcode }),
                type: "POST",
                cache : false ,
                success : function(response) {
                  console.log(response);
                  data = response;

                  if(data.success == true) {
                $.ajax({
                      url : "<?php echo base_url('Account/qr_data'); ?>",
                      data: { "USER_ID": data.data.USER_ID, 
                          "first_name": data.data.first_name,
                          "id_number": data.data.id_number
                        },
                      type: "POST",
                      cache : false ,
                       success : function(response2) {
                        if(response2 == 'ok'){
                           console.log(response2);   
                            location.href = "<?php echo base_url('Account/main');?>";
                        }else if(response2 == 'status0'){
                             alert("กรุณาติดต่อผู้ดูแลระบบ ในส่วนสิทธิการใช้งาน");
                              console.log(response2);   
                        }else{
                          console.log(response2);   
                        }      
                       }
                  });

              }else if(data.message == 'Expired'){
                deleteQR();
                location.href = "<?php echo base_url();?>";
              }else {
                loop = interV;
                return loop;
              }
                }
             });
        } , 4000 // Run ทุกๆ 10 วินาที

        );

    }

    function deleteQR() {
      $('#usernamlogin').show();
      $('#qrcodelogin').hide();
          var msg = $.ajax({
              url : "http://smartoffice.royalrain.go.th/services/web/index.php/user/delete_qr",
              data: JSON.stringify({ "qrcode": res.data.qrcode }),
              type: "POST",
              cache : false ,
              success : function(response) {
                console.log(response);
                data = response;
              }
           });
    }

    function clear_interval() {
      clearInterval(loop);
    }

    function forgetpass(){
      $('#usernamlogin').hide();
      $('#qrcodelogin').hide();
      $('#forgetpass').show();
    }


</script>

<body>

  <div class="container">
    <!-- Outer Row -->
    <br><br><br>
    <div class="row justify-content-center">
      <div class="col-xl-5 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <img src="<?php echo base_url('img/wing.png'); ?>" height=35% width=30%>
                    <h6></h6>
                    <h1 class="h4 text-gray-900 mb-4 "><black> ระบบสารสนเทศ</black></h1>
                  </div>
                  <hr>

                <div id="usernamlogin">
                   <form method="POST" action=" Account/authen ">
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" ><blue><i class="fas fa-user"></i></blue></span>
                        </div>
                        <input type="text" class=" form-control form-control-user" id="username" name="username" placeholder="ชื่อผู้ใช้งาน">
                      </div>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text " ><blue><i class="fas fa-key"></i></blue></span>
                        </div>
                        <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="รหัสผ่าน">
                      </div>
                      <br>
                      <button class="btn btn-outline-primary btn-user btn-block">เข้าสู่ระบบ</button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <button class="btn btn-outline-success btn-user btn-block" id="qr" name="qr1" href="#" onclick="generate_qr(); return false;">QR Code เข้าสู่ระบบ</button>
                  </div>
                  <div class="text-center">
                    <br>
                    <a class="small" href="#" onclick="forgetpass();">ลืมรหัสผ่าน</a>
                  </div>
                </div>

                  <div id="qrcodelogin" style="display:none">
                  <form method="POST" action="#">
                    <center>
                      <div id="qrcode" style="vertical-align: middle;"></div>
                    </center>
                  </form>
                  <hr>
                  <div class="text-center">
                    <button class="btn btn-outline-success btn-user btn-block"  href="#" onClick="window.location.reload();">เข้าสู่ระบบ ด้วยชื่อผู้ใช้</button>
                  </div>
                  <div class="text-center">
                    <br>
                    <a class="small" href="#" onclick="forgetpass();">ลืมรหัสผ่าน</a>
                  </div>
                </div>

                <div id="forgetpass" style="display:none">
                  <form method="POST" action=" <?php echo base_url('Account/forget_password'); ?> " >
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" ><blue><i class="fas fa-envelope"></i></blue></span>
                        </div>
                        <input type="text" class=" form-control form-control-user" id="email" name="email" placeholder="อีเมลล์">
                      </div>
                      <br>
                      <button class="btn btn-outline-primary btn-user btn-block">ยืนยัน</button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <button class="btn btn-outline-success btn-user btn-block"  href="#" onClick="window.location.reload();">เข้าสู่ระบบ</button>
                  </div>
                </div>

                </div>
              </div>
            </div>
        </div>
    </div>
  </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url(); ?>sbadmin2/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>sbadmin2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url(); ?>sbadmin2/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url(); ?>sbadmin2/js/sb-admin-2.min.js"></script>

</body>

</html>
