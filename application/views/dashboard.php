<html lang="en">

<body id="page-top">
                 
  <!-- Page Wrapper -->
  <div id="wrapper">
          <?php foreach($personal as $per){ ?>
            <!-- Card -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text font-weight text-primary text-uppercase mb-1"><?php echo $per['first_name']." ".$per['last_name'] ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-tag fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
      
  </div>

</body>
</html>
