
  <!-- Custom styles for this page -->
  <link href="<?php echo base_url(); ?>sbadmin2/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

<body id="page-top">
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h5 class="m-0 font-weight text-dark"> ตารางข้อมูลศูนย์ปฏิบัติการฝนหลวง หน่วยปฏิบัติการฝนหลวง สถานีเรดาร์ฝนหลวง และสนามบินในการปฏิบัติการฝนหลวง</h5><a target="_blank" href="https://www.royalrain.go.th/royalrain/ContactUs.aspx?MenuId=38">Official DRRAA</a> .
            </div>
            
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                <a class="btn btn-primary btn-sm" href="<?php echo base_url('Main_con/add'); ?>">เพิ่มข้อมูล</a><br><br>
                  <thead>
                    <tr>
                      <th>ลำดับ</th>
                      <th>ชื่อสถานี</th>
                      <th>ประเภท</th>
                      <th>ที่ตั้ง</th> 
                      <th>เบอร์ติดต่อ</th> 
                      <th>การดำเนินการ</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ลำดับ</th>
                      <th>ชื่อสถานี</th>
                      <th>ประเภท</th>
                      <th>ที่ตั้ง</th> 
                      <th>เบอร์ติดต่อ</th>
                      <th>การดำเนินการ</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php foreach ($contact as $con ) { ?>
                    <tr>
                      <th><?php echo $con['id']; ?></th>
                      <th><?php echo $con['name']; ?></th>
                      <th><?php if($con['type'] == 1){ ?> ศูนย์ปฏิบัติการฝนหลวง <?php } ?>
                          <?php if($con['type'] == 2){ ?> หน่วยปฏิบัติการฝนหลวง <?php } ?>
                          <?php if($con['type'] == 3){ ?> สถานีเรดาร์ <?php } ?>
                          <?php if($con['type'] == 4){ ?> สนามบิน <?php } ?></th>
                      <th><?php echo "อำเภอ".$con['district']." จังหวัด".$con['province']; ?></th> 
                      <th><?php echo $con['phone']; ?></th>
                      <th><a class="btn btn-primary btn-sm" href="<?php echo base_url('Main_con/edit'.'/'.$con['id']); ?>" role="button"><i class="fas fa-edit"></i></a> <a class="btn btn-danger btn-sm" href="<?php echo base_url('Main_con/contact_delete'.'/'.$con['id']); ?>" onClick="javascript:return confirm('คุณต้องการลบข้อมูลใช่หรือไม่');" ><i class="fas fa-trash-alt"></i></a></th>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>

  <!-- Page level plugins -->
  <script src="<?php echo base_url(); ?>sbadmin2/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>sbadmin2/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url(); ?>sbadmin2/js/demo/datatables-demo.js"></script>
