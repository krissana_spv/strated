<body id="page-top">

<div class="card">
  <div class="card-header">
    Quote
  </div>
  <div class="card-body">
    <form method="POST" action=" <?php echo base_url('Main_con/edit_data'); ?> ">
    <input type="hidden" name='id' value="<?php echo $contact['id'] ?>"> 
    <div class="form-row">
        <div class="form-group col-md-8">
        <label >ชื่อ</label>
        <input type="text" class="form-control" name='name' value="<?php echo $contact['name'] ?>"> 
        </div>
        <div class="form-group col-md-4">
        <label for="inputState">ประเภท</label>
        <select  class="form-control" name='type'>
            <option value="1" <?php if($contact['type'] == 1){ ?> selected <?php } ?> >ศูนย์ปฏิบัติการฝนหลวง</option>
            <option value="2" <?php if($contact['type'] == 2){ ?> selected <?php } ?> >หน่วยปฏิบัติการฝนหลวง</option>
            <option value="3" <?php if($contact['type'] == 3){ ?> selected <?php } ?> >สถานีเรดาร์</option>
            <option value="4" <?php if($contact['type'] == 4){ ?> selected <?php } ?> >สนามบิน</option>
        </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
        <label >อำเภอ</label>
        <input type="text" class="form-control" name='district' value="<?php echo $contact['district'] ?>">
        </div>
        <div class="form-group col-md-4">
        <label >จังหวัด</label>
        <input type="text" class="form-control" name='province' value="<?php echo $contact['province'] ?>">
        </div>
        <div class="form-group col-md-4">
        <label >เบอร์โทรศัพท์</label>
        <input type="text" class="form-control" name='phone' value="<?php echo $contact['phone'] ?>">
        </div>
    </div>
    <button type="submit" class="btn btn-primary">update</button>
    </form>
  </div>
</div>
</body>