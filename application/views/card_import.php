<body id="page-top">

<div class="card">
  <div class="card-header">
    เพิ่มข้อมูล
  </div>
  <div class="card-body">
    <form method="POST" action="Main_con/add_data">
    <div class="form-row">
        <div class="form-group col-md-8">
        <label >ชื่อ</label>
        <input type="text" class="form-control" name='name' require> 
        </div>
        <div class="form-group col-md-4">
        <label for="inputState">ประเภท</label>
        <select  class="form-control" name='type' require>
            <option >ประเภท</option>
            <option value="1"  >ศูนย์ปฏิบัติการฝนหลวง</option>
            <option value="2"  >หน่วยปฏิบัติการฝนหลวง</option>
            <option value="3"  >สถานีเรดาร์</option>
            <option value="4"  >สนามบิน</option>
        </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
        <label >อำเภอ</label>
        <input type="text" class="form-control" name='district' require>
        </div>
        <div class="form-group col-md-4">
        <label >จังหวัด</label>
        <input type="text" class="form-control" name='province' require>
        </div>
        <div class="form-group col-md-4">
        <label >เบอร์โทรศัพท์</label>
        <input type="text" class="form-control" name='phone' require>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">เพิ่มข้อมูล</button>
    </form>
  </div>
</div>
</body>